const express = require("express");

// Moongose is a package that allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulation our database
const mongoose = require("mongoose");

const app = express();

const port = 3001;
// MongoDB Atlas Connection
// when we want to use local mongoDB/robo3t
// mongoose.connect("mongodb://localhost:27017/databseName")

mongoose.connect("mongodb+srv://admin:admin@cluster0.ovosv.mongodb.net/batch164_todo?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

let db = mongoose.connection;

// console.lerror.bind(console) allows us to prin errors in the browser console and in the terminal
// connection error message
db.on("error", console.error.bind(console, "connection error"));
// connection is successful message
db.once("open", () => console.log("We're connected to the cloud database"));


app.use(express.json());

app.use(express.urlencoded({ extended: true}));


// Mongoose Schemas
// determines the structure of the documents to be written in the database
// act as blueprint to our data
// use Schema() constructor of Mongoose module to create a new Schema object
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		// default values are predefined values of a field if dont put any value.
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);


// 1. Create a User schema. 2. Create a User model.

const userSchema = new mongoose.Schema({
	uname: String,
	password: String,
	status: {
		type: String,
		// default values are predefined values of a field if dont put any value.
		default: "pending"
	}
})

const User = mongoose.model("User", userSchema);


// Routes/endpoints

// Creating a new task

// Business Logic
/*
1. add a functionality to check if there are duplicate tasks
	- if the task already exists in the database, we return error
	- if the task doesnt exist in database, we add it in db
		1. the task data will be coming from the request's body.
		2. create a new Task object with field/property.
		3. save the new object to our database.
*/

app.post("/tasks", (req, res) => {
	Task.findOne({ name: req.body.name }, (err, result) => {
		// If a document was found and the document's name matches the information sent via the client
		if(result != null && result.name == req.body.name) {
			return res.send("Duplicate task found")
		} else {
			// if no document was found
			// create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				// if there are errors in saving
				if(saveErr) {
					return console.error(saveErr)
				} else {
					return res.status(201).send("New task created")
				}
			})
		}
	})
})


// Get all tasks
// Business logic
/*
1. Find/retrieve all the documents
2. if an error is encoutered, print the error
3. if no errors are found, send a success status back to client and return an array of documents
*/
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				dataFromMDB: result
			})
		}
	})
})





app.post("/signup", (req, res) => {
	User.findOne({ uname: req.body.uname, password: req.body.password }, (err, result) => {
		
		if(result != null && result.uname == req.body.uname) {
			return res.send("Username already exist.")
		} else 
{		
			let newUser = new User({
				uname: req.body.uname,
				password: req.body.password
			});

			newUser.save((saveErr, savedUser) => {
				
				if(saveErr) {
					return console.error(saveErr)
				} else {
					return res.status(201).send("New user created")
				}
			})
		}
	})
})

app.get("/users", (req, res) => {
	User.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				dataFromMDB: result
			})
		}
	})
})


app.listen(port, () => console.log(`Server running at port ${port}`));